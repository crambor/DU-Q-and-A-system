- Interlinked with Zoom 
	- Behind uni login
	- Most people already use zoom
	- Zoom market place (all in one place)
- Is this just for remote or hybrid?


# Example Software
- Slack
- Slido
- Discord (for chat atleast)
- Stackoverflow (upvoting system))

# Additional Ideas
- Student led answers
	- lectures approve of student answers
	- lecturer answers are still upheld

- Voting on student anwsers (contribution score)
	- could increase engagement

- Hearing voting (Cna students hear the lecturer (technical issues))

- Draw feature (e.g chemistry drawing alkenes)
	- Either uploading images / draw function in app
	- Problems with moderation

# Things to worry about :(
- Moderation
- Seperation of QnA and Chat
- How to do timestamps with lecture recordings

- Back channel chat (admin chat)
- Authentication (Ask CIS about it)

- Allow admin to enable / disable features

# Implementation Ideas
- Main web app
	- Web app can accessed within some well known video conferencing apps (e.g zoom, teams)
	- Main web app can still be used standalone

# Stakeholders
- Students (maybe a google form)
- Dr Matt Wood ( [Matthew.J.Wood@durham.ac.uk](mailto:Matthew.J.Wood@durham.ac.uk)) (Maybe in person)
- CIS ( [servicedesk@durham.ac.uk](servicedesk@durham.ac.uk) )
- Demonstrators ( Craig )
	- Target other departments
		- E.g Latex things for maths (`MathJax.js`)






