# Matthew Woods
- Ask about "Could Have" requirements from *Requirements*
- Discuss options with CIS
- Scale
	- How many people
	- Concurrent lectures
- Use for in person lectures / online
- Schedule regular meetings to catch up and review
- Platform specifics
	- Zoom integration
- Remote or hybrid

# CIS
*NOTE:: Eamonn Bell would like to be in on this*
- Azure cloud storage
- How they handle internal web app integration
- University authentification
- Security procedure on storing student data