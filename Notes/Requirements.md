# Functional

>MVP:
> - Submit questions and answers
> 	- Lecturers can accept/reject questions/answers
> - Users can type in chat
> - Lecturers can answer questions
> - General chat
> 	- Seperate from QnA (Different Window)
> 	- Individual reply (DM/thread based)
> - Chat and QnA moderation
> - CIS integration (university login)
> - Different user roles (e.g student and lecturer)
> - Should work for both remote and in-person context

*Should Have:*
- Voting on student anwsers and questions (contribution score)
	- Only an upvote button (downvote may have immoral impacts)
	- could increase engagement

- Time stamping (Chat and questions)
	- Aligned with Encore video
	- Manually done by the lecturer (They click on the question they are answering and it gets time stamped)

- Allow admin to enable/disable features

- Export questions and answers
	- An API to interact with the system via a backend to pull out questions / chat to be used by the lecturer
		- Similar to OBS and Twitch Streaming for sub counts and other stats

- Have the recording (zoom / microsoft teams) be the backend, such that they are integrated into our system

- Keep the design modular for adding functionality or features such as the draw feature for visual aspects

*Could Have:*
- Guest access

- Student led answers
	- lectures approve of student answers
	- lecturer answers are still upheld

- Hearing voting (Can students hear the lecturer (technical issues))

- Draw feature (e.g chemistry drawing alkenes)
	- Either uploading images / draw function in app
	- Problems with moderation

- Back channel chat (admin chat)

- Download lecture QnA transcript

- Timeout / slow mode to prevent abuse or spam

# Non-Functional

>MVP:
> - [Usability] Support large number of users (~60 concurrent users)
> - [Availability] Device compatability
> - [Availability] Browser compatability
> - [Availability] Should work for both remote and in-person context
> - [Usability] Reasonable response time (~1 second)
> - [Security] Security
> 	- Authentication (Ask CIS about it)
> 	- Handle different user role permissions
> - [Regulatory] Let people know we are handling their data (GDPR)

# Risk and Issues
