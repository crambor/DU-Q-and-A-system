# Project: Q&A System for Online Webinars and Classes

### Problem:

Since the pandemic, an increasing number of classes and events (webinars) are being held
online, to capture a global cohort/audience. While video
conferencing has improved greatly
during this period, interactive elements have struggled to keep up. The ‘chat’ function often
ends up hidden within the user interface with limited functionality, and questions from the
cohort/audience often end up interspersed with general ‘chat’ from attendees. This adds to
the cognitive load of the presenter, as it can be distracting and difficult to decipher what a
genuine question is vs ‘general chit chat’. This can also be frustrating for attendees, whose
question (which can sometimes be important or pressing) can get lost in the liveliness of the
chat. This results in sometimes counter-productive practices, such as over-policing the chat,meaning attendees push their interaction to a 3rd party (unmoderated) platform such as
WhatsApp or Facebook messenger.

### Potential Solution:

For these reasons some practitioners push these interactions to a third-party platform such as 
Sli.do. However, this comes with significant problems, particularly in a university setting, as
this data is then being hosted by a third-party platform. Moreover, Sli.do has removed the
functionality for chat alongside asking and voting upon questions. This remains an issue for
practitioners wishing to host all interactive elements in a single app or location. There remains
a need for an all-in-one system that hosts the real-time interaction from an online event.

### Task:

To develop a Q&A/Chat system that could sit behind the university single-point-sign-in. This
system should have the ability for attendees to chat with one another, with ‘nested’ chat
(i.e.the ability to respond/react to a specific chat message). The system should have a separate
mechanism for Q&A from the audience/attendees. Attendees should be able to up/down vote
these questions so the event facilitator can assess the most/least popular questions. Thes
system should have the ability for the facilitator to accept or reject certain questions and
moderate chat messages.

### Extension Points:
- Backchannel chat for panellists of the webinar/event/class (which attendees can’t see). 
- Ability to timestamp questions so a recording can be later bookmarked by question. 
- Abilityto export questions into a format where they can be overlayed as a graphic. 
- Ability for guestaccess. 
- Separate Mobile/Desktop view.

### Contact:
Dr Matt Wood, DCAD: Matthew.J.Wood@durham.ac.uk